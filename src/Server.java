import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable{
	
	private BlockingQueue<Client> clients;
	private int waitingTime;
	private boolean stop;
	
	public Server(int maxClients) {
		this.clients =new ArrayBlockingQueue<Client>(maxClients);
		this.waitingTime = 0;
		this.stop=false;
	}

	public void addClient(Client newClient) {
		clients.add(newClient);
	}
	
	@Override
	public void run(){
		try {
		while(!stop) {
			while(this.clients.size()>0) {
					Thread.sleep(1000);
					if(this.clients.peek()!=null) {
						this.clients.peek().setServiceTime(this.clients.peek().getServiceTime()-1);
						this.waitingTime-=1;
						if(this.clients.peek().getServiceTime()==0) {
							this.clients.remove();
							this.waitingTime=0;
							Thread.sleep(1000);
						}
					}
			
				}
			}
		} catch (InterruptedException e) {
		}
	}

	public BlockingQueue<Client> getClients() {
		return clients;
	}

	public void setClients(BlockingQueue<Client> clients) {
		this.clients = clients;
	}

	public int getWaitingTime() {
		return waitingTime;
	}

	public void setWaitingTime(int waitingTime) {
		this.waitingTime = waitingTime;
	}
	
	
	public void stopServer() {
		stop=true;
	}

}
