
public class Client implements Comparable<Client> {
	private int ID;
	private int arrivalTime;
	private int serviceTime;

	
	public Client() {
		super();
	}

	public Client(int ID, int arrivalTime, int serviceTime) {
		super();
		this.ID = ID;
		this.arrivalTime = arrivalTime;
		this.serviceTime = serviceTime;
	}
	
	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public int getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public int getServiceTime() {
		return serviceTime;
	}

	public void setServiceTime(int serviceTime) {
		this.serviceTime = serviceTime;
	}

	@Override
	public String toString() {
		String str="("+this.ID+","+this.arrivalTime+","+this.serviceTime+")";
		return str;
	}

	@Override
	public int compareTo(Client client) {
		Integer item=this.getArrivalTime();
		return item.compareTo(client.getArrivalTime());
	}
	
	
	
}
