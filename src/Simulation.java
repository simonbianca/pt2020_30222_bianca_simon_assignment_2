import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Simulation{
	private String inputFile;
	private String outputFile;
	private ReadData data;
	private List<Client> clients;
	private List<Server> servers;
	private double averageWaitingTime;
	private int noOfClientsServed;
	
	public Simulation(String inputFile,String outputFile) {
	this.inputFile=inputFile;
	this.outputFile=outputFile;	
	this.data=new ReadData(inputFile);
	this.clients=generateClients();
	this.servers=new ArrayList<Server>(data.getNumberOfQueues());
	for(int index=0;index<data.getNumberOfQueues();index++) {
		servers.add(new Server(data.getNumberOfClients()));
	}
	this.averageWaitingTime=0;
	this.noOfClientsServed=0;
	}
	
	public int getRandomIntegerBetweenRange(int min, int max) {
		return (int)(Math.random()*((max-min)+1))+min;
	}
	public List<Client> generateClients(){
	List<Client> clients=new ArrayList<Client>();
	for(int index=1;index<=data.getNumberOfClients();index++) {
		clients.add(new Client(index,getRandomIntegerBetweenRange(data.getMinArrivalTime(),data.getMaxArrivalTime()),
								getRandomIntegerBetweenRange(data.getMinServiceTime(),data.getMaxServiceTime())));
		}
	Collections.sort(clients);
	return clients;
	}
	
	public void startSimulation() {
	     try {
	    	 PrintWriter newFile = new PrintWriter(outputFile);
	    	 newFile.print(" ");
	    	 newFile.close();
	        } catch (FileNotFoundException e) {
	        }
	     List<Thread> threads=new ArrayList<Thread>();
			threads=listOfThreads();
			for(Thread item:threads) {
				item.start();
			}
		writeData(outputFile,0,this.clients,this.servers);
		int simTime=1;
		while(simTime<=data.getSimulationTime()) {
			addClientsToQueue(simTime);
			writeData(outputFile,simTime,this.clients,this.servers);
			 try {
                 Thread.sleep(1000);
             } catch (InterruptedException e) {
                 e.printStackTrace();
             }
			simTime++;
		}
		if(simTime==data.getSimulationTime()+1) {
			averageWaitingTime/=noOfClientsServed;
			try {
				FileWriter file=new FileWriter(outputFile,true);
				file.append("\n\nAverage waiting time: "+averageWaitingTime);
				file.close();
			} catch (IOException e) {
			}
			
		}
		
		for(Server serv:this.servers) {
			serv.stopServer();
		}
	}
	
	public void writeData(String outputFile,int simTime,List<Client> list,List<Server> list1) {
		try {
			FileWriter file=new FileWriter(outputFile,true);
			file.append("\nTime "+simTime+"\nWaiting clients:");
				for(Client item: list) {
					file.append(item.toString()+";");
				}
			for(int index=0;index<data.getNumberOfQueues();index++){
				int rang=index+1;
				file.append("\nQueue "+rang+":");
				if(this.servers.get(index).getClients().size()>0) {
					for(Client item2:list1.get(index).getClients()) {
						file.append(item2.toString());
					}
				}
				else {
					file.append("CLOSED");
				}
			}
			file.close();
		} catch (IOException e) {
		}
	}
	public void addServers(Server server) {
		servers.add(server);
	}

	public void addClientsToQueue(int simTime) {
	  Iterator<Client> iterator = this.clients.iterator();
		while(iterator.hasNext()) {
			Client newClient=iterator.next();
			if(simTime==newClient.getArrivalTime()) {
				if(simTime+newClient.getServiceTime()<=data.getSimulationTime()) {
					int index=getRankQueue();
					this.servers.get(index).addClient(newClient);
					this.servers.get(index).setWaitingTime(newClient.getServiceTime());
					this.averageWaitingTime+=newClient.getServiceTime();
					this.noOfClientsServed++;
					iterator.remove();
				}
			}
		}
	}
	public int getRankQueue() {
		int wTime=Integer.MAX_VALUE;
		int rank=0;
		for(int index=0;index<data.getNumberOfQueues();index++) {
			if(wTime>this.servers.get(index).getWaitingTime()) {
				rank=index;
				wTime=this.servers.get(index).getWaitingTime();
			}
		}
	return rank;
	}
	public List<Thread> listOfThreads(){
		List<Thread> list=new ArrayList<Thread>();
		for(Server item:this.servers) {
			Thread t=new Thread(item);
			list.add(t);
		}
		return list;
	}

	public double getAverageWaitingTime() {
		return averageWaitingTime;
	}

	public int getNoOfClientsServed() {
		return noOfClientsServed;
	}
	
	
}