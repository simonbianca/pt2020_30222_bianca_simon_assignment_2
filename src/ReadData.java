import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ReadData {
	private int numberOfClients;
	private int numberOfQueues;
	private int simulationTime;
	private int minArrivalTime;
	private int maxArrivalTime;
	private int minServiceTime;
	private int maxServiceTime;
	
	public ReadData(String fileName) {
	List<String> data=getData(fileName);
	this.numberOfClients=Integer.parseInt(data.get(0));
	this.numberOfQueues=Integer.parseInt(data.get(1));
	this.simulationTime=Integer.parseInt(data.get(2));
	String[] str1=data.get(3).split(",");
	this.minArrivalTime=Integer.parseInt(str1[0]);
	this.maxArrivalTime=Integer.parseInt(str1[1]);
	String[] str2=data.get(4).split(",");
	this.minServiceTime=Integer.parseInt(str2[0]);
	this.maxServiceTime=Integer.parseInt(str2[1]);
	}
	
	public List<String> getData(String fileName){
		List<String> data=new ArrayList<String>();
		try {
			File file=new File(fileName);
			Scanner scanner=new Scanner(file);
			while(scanner.hasNextLine()) {
				data.add(scanner.nextLine());
			}
			scanner.close();
		}catch(FileNotFoundException e) {
			System.out.println("An error occurred.");
		}
		return data;
	}

	public int getNumberOfClients() {
		return numberOfClients;
	}

	public void setNumberOfClients(int numberOfClients) {
		this.numberOfClients = numberOfClients;
	}

	public int getNumberOfQueues() {
		return numberOfQueues;
	}

	public void setNumberOfQueues(int numberOfQueues) {
		this.numberOfQueues = numberOfQueues;
	}

	public int getSimulationTime() {
		return simulationTime;
	}

	public void setSimulationTime(int timeSimulation) {
		this.simulationTime = timeSimulation;
	}

	public int getMinArrivalTime() {
		return minArrivalTime;
	}

	public void setMinArrivalTime(int minArrivalTime) {
		this.minArrivalTime = minArrivalTime;
	}

	public int getMaxArrivalTime() {
		return maxArrivalTime;
	}

	public void setMaxArrivalTime(int maxArrivalTime) {
		this.maxArrivalTime = maxArrivalTime;
	}

	public int getMinServiceTime() {
		return minServiceTime;
	}

	public void setMinServiceTime(int minServiceTime) {
		this.minServiceTime = minServiceTime;
	}

	public int getMaxServiceTime() {
		return maxServiceTime;
	}

	public void setMaxServiceTime(int maxServiceTime) {
		this.maxServiceTime = maxServiceTime;
	}
	

}
